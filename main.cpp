#include <stdio.h>     // - Just for some ASCII messages
#include <GL/glut.h>   // - An interface and windows 
                       //   management library
#include "visuals.h"   // Header file for our OpenGL functions
#include <string.h>

#include <unistd.h>//for fork/exec

pid_t theme_id,thruster_id;  //process id for player that loops over theme sound and thrusters

extern model md;

char song[48];//name of song playing

////////////////// State Variables ////////////////////////


/////////////// Main Program ///////////////////////////

int main(int argc, char* argv[])
{ 
  // initialize GLUT library state
  glutInit(&argc, argv);
	
  // Set up the display using the GLUT functions to 
  // get rid of the window setup details:
  // - Use true RGB colour mode ( and transparency )
  // - Enable double buffering for faster window update
  // - Allocate a Depth-Buffer in the system memory or 
  //   in the video memory if 3D acceleration available	
                      //RGBA//DEPTH BUFFER//DOUBLE BUFFER//
  glutInitDisplayMode(GLUT_RGBA|GLUT_DEPTH|GLUT_DOUBLE);
 
  
  // Define the main window size and initial position 
  // ( upper left corner, boundaries included )
  glutInitWindowSize(1920,1080);
  glutInitWindowPosition(50,50);
  
  // Create and label the main window
  glutCreateWindow("Uriziel");
  
  // Configure various properties of the OpenGL rendering context
  Setup();
  
  // Callbacks for the GL and GLUT events:

  // The rendering function 
  glutDisplayFunc(Render);
  glutReshapeFunc(Resize);
  glutIdleFunc(Idle);
  glutKeyboardFunc(Keyboard);
  glutSpecialFunc(handleSpecialKeypress);
  glutMouseFunc(Mouse);
  
	if((theme_id=fork())==0)
		execlp("/usr/bin/mplayer","/usr/bin/mplayer","-loop","0","./tracks/main_theme1.wav",0);
	
	//infinitely play thrusters sound
	
	#if THRUSTER_SOUNDS==1
	if((thruster_id=fork())==0)
		execlp("/usr/bin/mplayer","/usr/bin/mplayer","-loop","0","./effects/thrusters.wav",0);
	#endif
	
	strcpy(song,"main_theme1.wav");
	
	ReadFile();
	
	glutCreateMenu(MenuSelect);
  	glutAddMenuEntry("Next Track",NEXT_TRACK);
	glutAddMenuEntry("Previous Track",PREVIOUS_TRACK);
	glutAddMenuEntry("Mute",MUTE);
	
	// attach the menu to the right button
  glutAttachMenu(GLUT_RIGHT_BUTTON);
	
  //Enter main event handling loop
  glutMainLoop();
  
  return 0;	
}  
