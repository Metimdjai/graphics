#define NEXT_TRACK 1
#define PREVIOUS_TRACK 2
#define MUTE 3

#define THRUSTER_SOUNDS 1	//set to 1 if the user wants to hear the ship's thrusters
#define DODGE_SOUNDS 1	//set to 1 if the user wants to hear the ship's dodging sounds

#define HOR_BARREL_ROLLS 1 //if set to 1, left/right barrel rolls will be activated
#define VERT_BARREL_ROLLS 1	//if set to 1, up/down barrel rolls will be activated

#define PLANE_POS 1//if set to 1, the plane will be moving realistically instead of staying at (0,0,0)

struct point 
{
	float x; 
	float y; 
	float z; 
};

struct face
{
	int vtx[3];
};

struct model
{
	point *obj_points;
	face *obj_faces;
	int vertices;
	int faces;
};


//-------- Functions --------------------------------

void Render();
// The function responsible for drawing everything in the 
// OpenGL context associated to a window. 

void Resize(int w, int h);
// Handle the window size changes and define the world coordinate 
// system and projection type

void Setup();
// Set up the OpenGL state machine and create a light source

void Idle();

void ReadFile(void);
//Function for reading a model file

void DisplayModel(void);
// Function for displaying a model

void Keyboard(unsigned char key,int x,int y);
// Function for handling keyboard events.

void handleSpecialKeypress(int key,int x,int y);

void Mouse(int button,int state,int x,int y); 
// Function for handling mouse events

void MenuSelect(int choice);
