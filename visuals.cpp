#include <stdio.h>     // - Just for some ASCII messages
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>

#include <random>//needed for rand generators

#include <GL/glut.h>   // - An interface and windows management libraryentered
#include "visuals.h"   // Header file for our OpenGL functions

//meteor model
model md;

#include <signal.h>//for kill/SIGKILL
#include <unistd.h>//for fork/exec

//process id for sound of theme and thrusters of ship
extern pid_t theme_id,thruster_id;

//current song playing
extern char song[20];

//Plane coordinates

static float tx = 0.0;
static float ty = 0.0;
static float tz = -20.0;

//Plane rotation coordinates (for scene rotation)
static float rotx = 0.0;
static float roty = 0.0;
static float rotz = 0.0;

//Plane rotation coordinates (for barrel rolls)
static float rotzz=0.0;
static float rotxx=0.0;

//bool for pausing the game
static bool gamepause=false;

//angle for propellers (it is being updated on each ObjPlane call, rotating them)
static float angle=0.0;

//counts how many times ObjPlane has been called. Since OpenGL renders 1 fps and 30 suffice to deceive the human eye, every 30 calls, the wing lights swap colours
static int entered=0;

//rr=true iff right wing's light is red
static bool rr=false;

//updates meteor angle so that it seems it is rotating on all axes
static float boxangle=0;

//initial speed of meteor
static float v=40.0;

//acceleration of meteor
static float TranslateY=200;

//scaling of oute sun sphere (being updated on each sun rendering)
static float sunScale=1.0;

//exitCond is true iff a game over condition is met (i.e. meteor collision)
static bool exitCond=false;

//these hold the values of the bounding box of the meteor model
float minx,miny,minz,maxx,maxy,maxz;

//Since meteor model is huge compared to the rest of the scene, it is scale to 0.01 of its original size. xscale, yscale and zscale randomly scale each dimension in [0,1].
static float xscale=1.0;
static float yscale=1.0;
static float zscale=1.0;

//direction from which a meteor will spawn
static int direction=1;

//counts how many meteor the player dodged
static int dodges=0;

using namespace std;

struct box{//struct of a bounding box
	float minx;
	float miny;
	float minz;
	float maxx;
	float maxy;
	float maxz;
}meteor; //since meteor will be constantly changing positions, it is defined as global

bool collision(box b1,box b2){	//a collision between two AABB's occurs iff b1 is inside b2 (and vice versa)
	return(b1.maxx>b2.minx&&b1.minx<b2.maxx&&b1.maxy>b2.miny&&b1.miny<b2.maxy&&b1.maxz>b2.minz&&b1.minz<b2.maxz);
}

//prints text
void text(const char *str,float size,float x,float y,float z)
{

	glPushMatrix();
	glTranslatef(x,y,z);
	glScalef(size,size,size);

	for (int i=0;i<(int)strlen(str);i++)
	  glutStrokeCharacter(GLUT_STROKE_ROMAN,str[i]);
	glPopMatrix();

}

//prints coordinates of the two bounding boxes passed as parameters
void print(box b1,box b2){
	printf("%f %f %f %f %f %f\n",b1.minx,b1.miny,b1.minz,b1.maxx,b1.maxy,b1.maxz);
	printf("%f %f %f %f %f %f\n",b2.minx,b2.miny,b2.minz,b2.maxx,b2.maxy,b2.maxz);
}

box ObjMeteor(float pos){
	box b;
	glPushMatrix();
	
	//set meteor colour (purple)
	glColor3f(0.6, 0.1, 0.5);
	
	//depending on the direction the meteor spawns,determine its bounding box
	if(direction==1){
		glTranslatef(pos,pos,pos);
		
		#if PLANE_POS==0
		
			b.minx=meteor.minx*0.02+pos-tx;
			b.miny=meteor.miny*0.02+pos-ty;
			b.minz=meteor.minz*0.02+pos-tz;
			b.maxx=meteor.maxx*0.02+pos-tx;
			b.maxy=meteor.maxy*0.02+pos-ty;
			b.maxz=meteor.maxz*0.02+pos-tz;
		
		#else
			
			b.minx=meteor.minx*0.02+pos;
			b.miny=meteor.miny*0.02+pos;
			b.minz=meteor.minz*0.02+pos;
			b.maxx=meteor.maxx*0.02+pos;
			b.maxy=meteor.maxy*0.02+pos;
			b.maxz=meteor.maxz*0.02+pos;
		
		#endif
	}	
	else if(direction==2){
		glTranslatef(pos,-pos,pos);
		
		#if PLANE_POS==0
		
			b.minx=meteor.minx*0.02+pos-tx;
			b.miny=meteor.miny*0.02-pos-ty;
			b.minz=meteor.minz*0.02+pos-tz;
			b.maxx=meteor.maxx*0.02+pos-tx;
			b.maxy=meteor.maxy*0.02-pos-ty;
			b.maxz=meteor.maxz*0.02+pos-tz;
		
		#else
		
			b.minx=meteor.minx*0.02+pos;
			b.miny=meteor.miny*0.02-pos;
			b.minz=meteor.minz*0.02+pos;
			b.maxx=meteor.maxx*0.02+pos;
			b.maxy=meteor.maxy*0.02-pos;
			b.maxz=meteor.maxz*0.02+pos;
		
		#endif
	}	
	else if(direction==3){	
		glTranslatef(-pos,pos,pos);
		
		#if PLANE_POS==0
		
			b.minx=meteor.minx*0.02-pos-tx;
			b.miny=meteor.miny*0.02+pos-ty;
			b.minz=meteor.minz*0.02+pos-tz;
			b.maxx=meteor.maxx*0.02-pos-tx;
			b.maxy=meteor.maxy*0.02+pos-ty;
			b.maxz=meteor.maxz*0.02+pos-tz;
		
		#else
		
			b.minx=meteor.minx*0.02-pos;
			b.miny=meteor.miny*0.02+pos;
			b.minz=meteor.minz*0.02+pos;
			b.maxx=meteor.maxx*0.02-pos;
			b.maxy=meteor.maxy*0.02+pos;
			b.maxz=meteor.maxz*0.02+pos;
		
		#endif	
	}	
	else{
		glTranslatef(-pos,-pos,pos);
		
		#if PLANE_POS==0
		
			b.minx=meteor.minx*0.02-pos-tx;
			b.miny=meteor.miny*0.02-pos-ty;
			b.minz=meteor.minz*0.02+pos-tz;
			b.maxx=meteor.maxx*0.02-pos-tx;
			b.maxy=meteor.maxy*0.02-pos-ty;
			b.maxz=meteor.maxz*0.02+pos-tz;
		
		#else
		
			b.minx=meteor.minx*0.02-pos;
			b.miny=meteor.miny*0.02-pos;
			b.minz=meteor.minz*0.02+pos;
			b.maxx=meteor.maxx*0.02-pos;
			b.maxy=meteor.maxy*0.02-pos;
			b.maxz=meteor.maxz*0.02+pos;
		
		#endif
	}
	
	//it will rotate only if the game is not paused	
	if(!gamepause)
		boxangle+=10.0f;
		
	#if PLANE_POS==0
	
		glTranslatef(-tx,-ty,-tz);//all objects move instead of the plane!
		
	#endif
		
	
	//rotate according to boxangle
	glRotatef(boxangle,1,1,1);
	
	//scale model to 0.01 of initial size and then xscale, yscale, zscale of that to further randomize each meteor
	glScalef(0.02*xscale,0.02*yscale,0.02*zscale);
	
	//actual rendering of the model
	DisplayModel();
	
	glPopMatrix();
	
	//if the player is still alive, they successfully dodged yet another meteor
			
	if(!exitCond&&!gamepause&&pos<=-200){
		dodges++;
	}	
	
	return b;
}

box ObjPlane(){
	box b;
	glPushMatrix();

	#if PLANE_POS==1
	
		glTranslatef(tx,ty,tz);
	
	#endif

	//if barrel rolls are activated,and depending on which of the two, perform proper rotations
	#if HOR_BARREL_ROLLS==1
	
		glRotatef(rotzz,0,0,1);
		
	#endif
	
	#if VERT_BARREL_ROLLS==1
	
		glRotatef(rotxx,1,0,0);
		
	#endif
	
	//start plane design
	
	//front wings		
	glPushMatrix();
	glColor3f(0.6, 0.0, 0.0);
	glRotatef(270,1,0,0);
	glScalef(5.0f,1.0f,1.0f);
	glutSolidCone(10,3,90,90);
	glPopMatrix();
	
	//body
	glPushMatrix();
	glColor3f(0.0, 0.0, 1.0);
	glTranslatef(0,-4.0,14.0);
	glScalef(0.7f,0.7f,6.0f);
	glutSolidTorus(6.0,3.0,90,90);
	glPopMatrix();
	
	//body enhancement
	glPushMatrix();
	glTranslatef(0,-3,1.0);
	glScalef(1.15f,0.5f,1.8f);
	glColor4f(0.0, 0.0, 1.0,1.0);
	glutSolidSphere(15.0,90,90);
	glPopMatrix();
	
	//back wings
	glPushMatrix();
	glTranslatef(0,-1.0,43.0);
	glRotatef(270,1,0,0);
	glScalef(1.5f,0.5f,0.7f);
	glColor4f(1.0, 0.0, 0.0,1.0);
	glutSolidCone(10,3,90,90);
	glPopMatrix();
	
	//back wings cone
	glPushMatrix();
	glTranslatef(0,-3.0,43.0);
	glRotatef(30,1,0,0);
	glRotatef(270,1,0,0);
	glScalef(0.43f,0.75f,4.5f);
	glColor4f(1.0, 0.0, 0.0,1.0);
	glutSolidCone(10,3,90,90);
	glPopMatrix();
	
	//pilot's head
	glPushMatrix();
	glTranslatef(0,8,2.0);
	glRotatef(260,1,0,0);
	glRotatef(90,0,0,1);
	glScalef(0.5f,0.5f,0.5f);
	glColor3f(1.0, 1.0, 0.0);
	glutSolidSphere(5.0,90,90);
	glPopMatrix();
	
	//pilot's body
	glPushMatrix();
	glTranslatef(0,5,2.0);
	glRotatef(260,1,0,0);
	glRotatef(90,0,0,1);
	glScalef(0.4f,0.4f,0.4f);
	glColor3f(1.0, 1.0, 0.0);
	glutSolidSphere(5.0,90,90);
	glPopMatrix();
	
	//pilot's right arm
	glPushMatrix();
	glTranslatef(2.0,5,-2.0);
	glRotatef(-30,0,1,0);
	glRotatef(260,1,0,0);
	glRotatef(90,0,0,1);
	glScalef(0.8f,0.2f,0.2f);
	glColor3f(1.0, 1.0, 0.0);
	glutSolidSphere(5.0,90,90);
	glPopMatrix();
	
	//pilot's left arm
	glPushMatrix();
	glTranslatef(-2.0,5,-2.0);
	glRotatef(30,0,1,0);
	glRotatef(260,1,0,0);
	glRotatef(90,0,0,1);
	glScalef(0.8f,0.2f,0.2f);
	glColor3f(1.0, 1.0, 0.0);
	glutSolidSphere(5.0,90,90);
	glPopMatrix();
	
	//plane's wheel
	glPushMatrix();
	glTranslatef(0.0,5,-4.0);
	glRotatef(90,0,0,1);
	glScalef(0.4f,0.4,0.2f);
	glColor3f(1.0, 0.0, 0.0);
	glutSolidTorus(5.0,4.0,90,90);
	glPopMatrix();
	
	//cockpit
	glPushMatrix();
	glTranslatef(0,1,-2.0);
	glRotatef(260,1,0,0);
	glRotatef(90,0,0,1);
	glScalef(1.0f,0.65f,0.7f);
	glColor4f(0.0, 1.0, 0.0,0.6);
	glutSolidSphere(15.0,90,90);
	glPopMatrix();
	
	//propeller 1 support
	glPushMatrix();
	glTranslatef(35,-2,-3.0);
	glRotatef(270,1,0,0);
	glRotatef(90,0,0,1);
	glScalef(0.65f,0.2f,0.1f);
	glColor3f(0.0, 0.0, 0.9);
	glutSolidSphere(15.0,90,90);
	glPopMatrix();
	
	//propeller 1
	glPushMatrix();
	glTranslatef(35,-2,-10.0);
	glRotatef(270,1,0,0);
	glRotatef(90,0,0,1);
	if(!gamepause)
		angle+=15.0f;
	glRotatef(angle,1,0,0);
	glScalef(0.1f,0.6f,0.1f);
	glColor4f(0.0, 1.0, 0.0,0.6);
	glutSolidSphere(15.0,90,90);
	glPopMatrix();
	
	if(!gamepause)
		entered++;
	
	//right wing light
	glPushMatrix();
	glTranslatef(47,0.0,0.0);
	glRotatef(270,1,0,0);
	glRotatef(90,0,0,1);
	glScalef(0.25f,0.2f,0.1f);
	if(entered<=60){//if 60 frames have been rendered,swap wing light colours
		if(rr==true)
			glColor4f(1.0, 0.0, 0.0,0.7);
		else
			glColor4f(1.0, 1.0, 0.0,0.7);	
	}	
	glutSolidSphere(15.0,90,90);
	glPopMatrix();
	
	//propeller 2 support
	glPushMatrix();
	glTranslatef(-35,-2,-3.0);
	glRotatef(270,1,0,0);
	glRotatef(90,0,0,1);
	glScalef(0.65f,0.2f,0.1f);
	glColor3f(0.0, 0.0, 0.9);
	glutSolidSphere(15.0,90,90);
	glPopMatrix();
	
	//propeller 2
	glPushMatrix();
	glTranslatef(-35,-2,-10.0);
	glRotatef(270,1,0,0);
	glRotatef(90,0,0,1);
	glRotatef(angle,1,0,0);
	glScalef(0.1f,0.6f,0.1f);
	glColor4f(0.0, 1.0, 0.0,0.6);
	glutSolidSphere(15.0,90,90);
	glPopMatrix();
	
	//left wing light
	glPushMatrix();
	glTranslatef(-47,0.0,0.0);
	glRotatef(270,1,0,0);
	glRotatef(90,0,0,1);
	glScalef(0.25f,0.2f,0.1f);
	if(entered<=60){//if 60 frames have been rendered,swap wing light colours
		if(rr==false)
			glColor4f(1.0, 0.0, 0.0,0.7);
		else
			glColor4f(1.0, 1.0, 0.0,0.7);
	}
	else{//60 frames have passed, reverse colour meaning for the next 60 frames and reset entered
		rr=!rr;
		entered=0;
	}	
	glutSolidSphere(15.0,90,90);
	glPopMatrix();
	
	glPopMatrix();
	
	//calculate bounding box of plane. Const numbers are the minimum bounding box for the plane if it was centered at (0,0,0)
	
	#if PLANE_POS==0
		b.minx=-50;
		b.miny=-15;
		b.minz=-40;
		b.maxx=50;
		b.maxy=15;
		b.maxz=40;
	
	#else
	
		b.minx=-50+tx;
		b.miny=-15+ty;
		b.minz=-40+tz;
		b.maxx=50+tx;
		b.maxy=15+ty;
		b.maxz=40+tz;
	
	#endif
	
	return b;
}

bool inBound(box b){
	//checks if bounding box's depth is in the scene
	return(b.minz>1.0&&b.maxz<500);
}

void ObjStar(float r,float g,float b,float x,float y,float z, float pos){
	glPushMatrix();
	if(!gamepause)//if the game is not paused,change the colour
		glColor3f(r,g,b);
		
	//generate a distribution of reals in [0,500] and a distribution of ints in [1,2]	
	std::random_device seed;
	std::mt19937 gen(seed());
	std::uniform_real_distribution<>dis(0,500);
	std::uniform_int_distribution<>s(1,2);
	
	if(!gamepause){
		//randomly generate star positions 
		//for z dimension we need to "fairly" distribute stars in front and back from the plane
		float zpos=dis(gen);
		if(s(gen)==2)
			glTranslatef(dis(gen),dis(gen),zpos*1);	
		else	
			glTranslatef(dis(gen),dis(gen),zpos*(-1));
		glScalef(x,y,z);	
	}	
	glutSolidSphere(2.0,40.0,40.0);
	glPopMatrix();
}

void ObjSun(){
	//set sun's position in the background
	glTranslatef(295,145,-195);
	
	//or uncomment those to make it move with the plane!
//		glTranslatef(-tx,-ty,-tz);

	//if the outer sun sphere has gotten too large
	if(sunScale>1.4){
		if(!gamepause)//if the game is not paused
			sunScale=0;//set it back to 0
	}		
	else{
		if(!gamepause)//if the game is not paused, slightly increase its scale
			sunScale+=0.01;			
	}		
	
		glPushMatrix();
		
		//render inner sphere
		glColor4f(1.0,0.6, 0.0,0.8);
		glutSolidSphere(45.0,90.0,90.0);
		glPopMatrix();
	
		glPushMatrix();
			
		//render outer sphere with colour transparency based on scale size			
		glScalef(sunScale,sunScale,sunScale);		
		glColor4f(0.9,0.4, 0.0,sunScale-0.6);
		glutSolidSphere(45.0,90.0,90.0);
		glPopMatrix();
	
}

void Render()
{    
  //CLEARS FRAME BUFFER ie COLOR BUFFER& DEPTH BUFFER (1.0)
  
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // Clean up the colour of the window
													   // and the depth buffer
	glMatrixMode(GL_MODELVIEW); 
	glLoadIdentity();
	if(!exitCond){//render regular stuff if the game is not finished (i.e. having crashed on a meteor)
		if(gamepause){//if paused, print pause message
		  		text("Game Paused!",0.07f,-50,80,-400);
		  		text("Press p to resume game",0.07f,-50,70,-400);
		  }
	
		glTranslatef(0,0,-200);//move the scene camera a bit backwards
	
		//perform proper scene rotations according to button presses
		glRotatef(rotx,1.0,0,0);
		glRotatef(roty,0,1.0,0);
		glRotatef(rotz,0,0,1.0);	
	
		//render plane and get its bounding box
		box planeBox=ObjPlane();
	
		//same for the meteor
		box meteorBox=ObjMeteor(-TranslateY);
	
		//generate a distribution of reals in [0,1] to randomly pick rgb and scaling factors
		std::random_device seed;
		std::mt19937 gen(seed());
		std::uniform_real_distribution<>dis(0,1);
	
		float r,g,b;
		float x,y,z;
	
		r=dis(gen);
		g=dis(gen) ;
		b=dis(gen) ;
	
		x=dis(gen);
		y=dis(gen) ;
		z=dis(gen) ;
	
		//generate sun
		ObjSun();
	
		for(int i=-7;i<7;i++){//render 196 stars randomly in the scene per render call
			for(int j=-7;j<7;j++){
				glPushMatrix();
				int s1,s2;
				s1=dis(gen)>=0.5?1:-1;
				s2=dis(gen)>=0.5?1:-1;
				if(!gamepause)//if the game is not paused, move stars
					glTranslatef(s1*dis(gen)*1000,s2*dis(gen)*1000,-dis(gen)*100);
				ObjStar(r,g,b,x,y,z,TranslateY);//and generate them
				glPopMatrix();
			}
		}
		if(collision(meteorBox,planeBox)){//if a collision occured, print the coordinates of the bounding boxes
				print(meteorBox,planeBox);
				exitCond=true;	//and set exitCond to true
		}
	}
	else{	
		//exitCond reached, print score message, thanking the player for trying the game, simply click to exit
		char buf[100];
		sprintf(buf,"%d %s",dodges,"meteors");
			
		text("You successfully dodged:",0.3f,-200,70,-400);
		text(buf,0.3f,-200,25,-400);
		text("Thanks for playing!",0.3f,-200,-15,-400);
  		text("Click to exit!",0.3f,-200,-60,-400);
	}		
	glutSwapBuffers();             // All drawing commands applied to the 
                                 // hidden buffer, so now, bring forward
                                 // the hidden buffer and hide the visible one
}

//-----------------------------------------------------------

void Resize(int w, int h)
{ 
  // define the visible area of the window ( in pixels )
  if (h==0) h=1;
  glViewport(0,0,w,h); 

  // Setup viewing volume

  glMatrixMode(GL_PROJECTION); 
  glLoadIdentity();
 
  gluPerspective(60.0, (float)w/(float)h, 1.0, 500.0);
}

void Idle()
{
	if(!gamepause){//if the game is not paused
		//update acceleration of meteor
		float g=10;
		float dt=0.01;
		if(TranslateY>=-200){//if the meteor is still in bounds
			//let it continue its course
			v=v+g*dt;
			TranslateY=TranslateY-v*dt;
		}	
		else{
			//randomly decide one of the four screen corners to "spawn" a new meteor, which will simply be the current one moved way back
			std::random_device seed;
			std::mt19937 gen(seed());
			std::uniform_int_distribution<>dis(1,4);
			direction=dis(gen);
			TranslateY=200;
			v=40;
			
			//randomly scale the "new" meteor's 3 dimensions
			std::uniform_real_distribution<>scaledis(0.7,1);
			xscale=scaledis(gen);
			yscale=scaledis(gen);
			zscale=scaledis(gen);
			
		}
		glutPostRedisplay();
	}	
}

void Keyboard(unsigned char key,int x,int y){
	//regular keys handler. Those will update variables iff the game is not paused
	switch(key)
	{
	case 'q' : 
	//Quitting will free resources if exitCond is not met so far
		if(!exitCond){
			free(md.obj_points);
 			free(md.obj_faces);
			if(song[0]!='\0') 			
	 			kill(theme_id,SIGKILL);
 			#if THRUSTER_SOUNDS==1
 			
 				kill(thruster_id,SIGKILL);
 				
 			#endif
			exit(0);
		}	
	case 'w' : 
		if(!gamepause)
			rotx -=10.5f;
		break;		
	case 's' : 
		if(!gamepause)
			rotx+=10.5f;
		break;		
	case 'd' : 
		if(!gamepause)
			roty-=10.5f;
		break;		
	case 'a' : 
		if(!gamepause)
			roty+=10.5f;
		break;		
	case 'p': 
		if(!gamepause)
			gamepause=true;
		else
			gamepause=false;	
		break;	
	default : break;
	}
	
	glutPostRedisplay();	
}

void handleSpecialKeypress(int key,int x,int y)
{
	//special keys handler, uses arrow keys to move plane
	switch(key)
	{
	case GLUT_KEY_LEFT : 
		if(!gamepause){
			#if PLANE_POS==1
			
				if(tx>=-200.0f+2.0f){
					
					tx-=2.0f;
					if(rotzz<=30)
						rotzz+=5.5f;
					#if DODGE_SOUNDS==1
			
						if((int)tx%70==0)
							system("mplayer ./effects/dodge.wav&");
					
					#endif
				}			
			
			#else
			
				tx-=2.0f;
				if(rotzz<=30)
					rotzz+=5.5f;
				#if DODGE_SOUNDS==1
			
					if((int)tx%70==0)
						system("mplayer ./effects/dodge.wav&");
				
				#endif
			
			#endif
		}	
		break;
	case GLUT_KEY_RIGHT : 
		if(!gamepause){
			#if PLANE_POS==1
			
				if(tx<=200.0f){				
					tx+=2.0f;
					if(rotzz>=-30)
						rotzz-=5.5f;
					#if DODGE_SOUNDS==1
			
						if((int)tx%70==0)
							system("mplayer  ./effects/dodge.wav&");
				
					#endif
				}
			
			#else
			
				tx+=2.0f;
				if(rotzz>=-30)
					rotzz-=5.5f;
				#if DODGE_SOUNDS==1
			
					if((int)tx%70==0)
						system("mplayer  ./effects/dodge.wav&");
				
				#endif	
				
			#endif	
		}	
		break;
	case GLUT_KEY_UP: 
		if(!gamepause){
			#if PLANE_POS==1
			
				if(ty<=100.0f){
					ty+=2.0f;
					if(rotxx<=15)
						rotxx+=5.5f;
					#if DODGE_SOUNDS==1
			
						if((int)ty%70==0)
							system("mplayer  ./effects/dodge.wav&");
					
					#endif	
				}
				
			#else
			
				ty+=2.0f;
				if(rotxx<=15)
					rotxx+=5.5f;
				#if DODGE_SOUNDS==1
			
					if((int)ty%70==0)
						system("mplayer  ./effects/dodge.wav&");
					
				#endif	
				
			#endif	
		}	
		break;
	case GLUT_KEY_DOWN : 
		if(!gamepause){
			#if PLANE_POS==1
				
				if(ty>=-100.f+2.0f){
					ty-=2.0f;
					if(rotxx>=-15)
						rotxx-=5.5f;
					#if DODGE_SOUNDS==1
			
						if((int)ty%70==0)
							system("mplayer  ./effects/dodge.wav&");
					
					#endif	
				}
				
			#else
			
				ty-=2.0f;
				if(rotxx>=-15)
					rotxx-=5.5f;
				#if DODGE_SOUNDS==1
			
					if((int)ty%70==0)
						system("mplayer  ./effects/dodge.wav&");
					
				#endif	
				
			#endif	
		}	
		break;
	default : break;
	}

	glutPostRedisplay();

}

void Mouse(int button,int state,int x,int y)
{
	 if(state == GLUT_DOWN && button == GLUT_LEFT_BUTTON)
	 {
		 if(exitCond){//player left clicked and game was already over, we can now free all allocated memory and exit!
		 		//free memory for arrays of meteor model
		 		free(md.obj_points);
		 		free(md.obj_faces);
		 		if(song[0]!='\0')
			 		kill(theme_id,SIGKILL);
		 		#if THRUSTER_SOUNDS==1
 				kill(thruster_id,SIGKILL);
 				#endif
		 		exit(0);
		 }
         glutPostRedisplay();
	 }
}

void MenuSelect(int choice)
{
	switch (choice) {
		case NEXT_TRACK : 
			if(!strcmp(song,"./tracks/main_theme1.wav")){
				kill(theme_id,SIGKILL);
				if((theme_id=fork())==0)
					execlp("/usr/bin/mplayer","/usr/bin/mplayer","-loop","0","./tracks/main_theme2.wav",0);
				strcpy(song,"./tracks/main_theme2.wav");
			}
			else if(!strcmp(song,"./tracks/main_theme2.wav")){
				kill(theme_id,SIGKILL);
				if((theme_id=fork())==0)
					execlp("/usr/bin/mplayer","/usr/bin/mplayer","-loop","0","./tracks/main_theme3.wav",0);
				strcpy(song,"./tracks/main_theme3.wav");
			}
			else if(!strcmp(song,"./tracks/main_theme3.wav")){
				kill(theme_id,SIGKILL);
				if((theme_id=fork())==0)
					execlp("/usr/bin/mplayer","/usr/bin/mplayer","-loop","0","./tracks/main_theme4.wav",0);
				strcpy(song,"./tracks/main_theme4.wav");
			}
			else{
				kill(theme_id,SIGKILL);
				if((theme_id=fork())==0)
					execlp("/usr/bin/mplayer","/usr/bin/mplayer","-loop","0","./tracks/main_theme1.wav",0);
				strcpy(song,"./tracks/main_theme1.wav");
			}
			break;
		case PREVIOUS_TRACK : 
			if(!strcmp(song,"./tracks/main_theme1.wav")){
				kill(theme_id,SIGKILL);
				if((theme_id=fork())==0)
					execlp("/usr/bin/mplayer","/usr/bin/mplayer","-loop","0","./tracks/main_theme4.wav",0);
				strcpy(song,"./tracks/main_theme4.wav");
			}
			else if(!strcmp(song,"./tracks/main_theme2.wav")){
				kill(theme_id,SIGKILL);
				if((theme_id=fork())==0)
					execlp("/usr/bin/mplayer","/usr/bin/mplayer","-loop","0","./tracks/main_theme1.wav",0);
				strcpy(song,"./tracks/main_theme1.wav");
			}
			else if(!strcmp(song,"./tracks/main_theme3.wav")){
				kill(theme_id,SIGKILL);
				if((theme_id=fork())==0)
					execlp("/usr/bin/mplayer","/usr/bin/mplayer","-loop","0","./tracks/main_theme2.wav",0);
				strcpy(song,"./tracks/main_theme2.wav");
			}
			else{
				kill(theme_id,SIGKILL);
				if((theme_id=fork())==0)
					execlp("/usr/bin/mplayer","/usr/bin/mplayer","-loop","0","./tracks/main_theme3.wav",0);
				strcpy(song,"./tracks/main_theme3.wav");
			}
			break;
		case MUTE: 
			kill(theme_id,SIGKILL);
			song[0]='\0';
			break;
	}

}

void Setup()
{ 
	//Parameter handling
	glShadeModel (GL_SMOOTH);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);  //renders a fragment if its z value is less or equal of the stored value
	glClearDepth(1);
    
	// polygon rendering mode
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial( GL_FRONT, GL_AMBIENT_AND_DIFFUSE );
  
  	//move light source to sun position
  	GLfloat light_position[]={205.0,105.0,-195.0,0.7};
	glLightfv( GL_LIGHT0, GL_POSITION, light_position);

	GLfloat ambientLight[]={0.3,0.3,0.3,1.0};
	GLfloat diffuseLight[]={0.8,0.8,0.8,1.0};
	GLfloat specularLight[]={1.0,1.0,1.0,1.0};
  
  	//use golden specrefs for sun
  
  	glLightfv( GL_LIGHT0, GL_SPECULAR, specularLight);
	GLfloat specref[4];
	specref[0]=0.247;specref[1]=0.225;specref[2]=0.065;specref[3]=1.0;
	glMaterialfv(GL_FRONT,GL_AMBIENT,specref);
	specref[0]=0.346;specref[1]=0.314;specref[2]=0.090;specref[3]=1.0;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,specref);
	specref[0]=0.797;specref[1]=0.724;specref[2]=0.208;specref[3]=1.0;
	glMaterialfv(GL_FRONT,GL_SPECULAR,specref);
	glMaterialf(GL_FRONT,GL_SHININESS,83.2);
  
	glLightfv( GL_LIGHT0, GL_AMBIENT, ambientLight );
	glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuseLight );
	

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);

	glFrontFace(GL_CCW);
 
 	glEnable(GL_BLEND);
 	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Black background
	glClearColor(0.0f,0.0f,0.0f,0.0f);

}

void ReadFile(void)
{
	minx=miny=minz=maxx=maxy=maxz=0;
	FILE *fp=fopen("./models/asteroid.obj","r");
	if(fp==NULL)
		exit(1);
		
	md.vertices=md.faces=0;		
	
	char buf[200];
	
	while(getc(fp) != EOF){
		fseek(fp,-1,SEEK_CUR);
		fgets(buf,200,fp);
		if(buf[0]=='v'&&buf[1]!='n')
			md.vertices++;
		if(buf[0]=='f')
			md.faces++;	
	}
	md.obj_points=(point *)malloc(md.vertices*sizeof(point));
	md.obj_faces=(face *)malloc(md.faces*sizeof(face));
	
	if(md.obj_points==NULL||md.obj_faces==NULL){
		perror("malloc");
		exit(0);
	}
	
	fseek(fp,0,SEEK_SET);
	for(int i=0;i<md.vertices;i++){
		fgets(buf,200,fp);
		if(buf[0]!='v'||(buf[0]=='v'&&buf[1]=='n')){
			i--;
			continue;
		}
		sscanf(buf,"v %e %e %e\n",&md.obj_points[i].x,&md.obj_points[i].y,&md.obj_points[i].z);
		if(md.obj_points[i].x<minx)
			minx=md.obj_points[i].x;
		if(md.obj_points[i].x>maxx)
			maxx=md.obj_points[i].x;
			
		if(md.obj_points[i].y<miny)
			miny=md.obj_points[i].y;
		if(md.obj_points[i].y>maxy)
			maxy=md.obj_points[i].y;
			
		if(md.obj_points[i].z<minz)
			minz=md.obj_points[i].z;
		if(md.obj_points[i].z>maxz)
			maxz=md.obj_points[i].z;			
	}	
	
	for(int i=0;i<md.faces;i++){
		fgets(buf,200,fp);
		if(buf[0]!='f'){
			i--;
			continue;
		}
		sscanf(buf,"f %d//%d %d//%d %d//%d\n",&md.obj_faces[i].vtx[0],&md.obj_faces[i].vtx[0],&md.obj_faces[i].vtx[1],&md.obj_faces[i].vtx[1],&md.obj_faces[i].vtx[2],&md.obj_faces[i].vtx[2]);
	}	
		
//	printf("BOUNDING BOX =(%f,%f,%f),(%f,%f,%f)\n",minx,miny,minz,maxx,maxy,maxz);	

	meteor.minx=minx;
	meteor.miny=miny;
	meteor.minz=minz;
	meteor.maxx=maxx;
	meteor.maxy=maxy;
	meteor.maxz=maxz;
	fclose(fp);
}

void DisplayModel(void)
{

	glPushMatrix();
	glBegin(GL_TRIANGLES);

	for (int i = 0; i < md.faces; i++)
	{
		glVertex3f(md.obj_points[md.obj_faces[i].vtx[0]-1].x,md.obj_points[md.obj_faces[i].vtx[0]-1].y,md.obj_points[md.obj_faces[i].vtx[0]-1].z);
		glVertex3f(md.obj_points[md.obj_faces[i].vtx[1]-1].x,md.obj_points[md.obj_faces[i].vtx[1]-1].y,md.obj_points[md.obj_faces[i].vtx[1]-1].z);
		glVertex3f(md.obj_points[md.obj_faces[i].vtx[2]-1].x,md.obj_points[md.obj_faces[i].vtx[2]-1].y,md.obj_points[md.obj_faces[i].vtx[2]-1].z);
	}

	glEnd();
	glPopMatrix();

}
