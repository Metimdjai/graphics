CC = g++
CCFLAGS = -Wall -g -std=c++11
LIBS = -lGL -lGLEW -lGLU -lglut

OBJECTS=main.o visuals.o

default: uriziel main.o visuals.o
all: default

uriziel: $(OBJECTS)
	$(CC) $(OBJECTS) $(LIBS) -o $@

main.o: main.cpp visuals.h
	$(CC) $(CCFLAGS) -c $< -o $@
	
visuals.o: visuals.cpp visuals.h
	$(CC) $(CCFLAGS) -c $< -o $@
	
clean:
	rm -rf main.o visuals.o
	
clear:			
	rm -rf main.o visuals.o uriziel
